﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Facturation.Models;

namespace API_Facturation.Controllers
{
    public class LigneFacturesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/LigneFactures
        public IQueryable<LigneFacture> GetLigneFactures()
        {
            return db.LigneFactures;
        }

        // GET: api/LigneFactures/5
        [ResponseType(typeof(LigneFacture))]
        public IHttpActionResult GetLigneFacture(int id)
        {
            LigneFacture ligneFacture = db.LigneFactures.Find(id);
            if (ligneFacture == null)
            {
                return NotFound();
            }

            return Ok(ligneFacture);
        }

        // PUT: api/LigneFactures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLigneFacture(int id, LigneFacture ligneFacture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ligneFacture.Id)
            {
                return BadRequest();
            }

            db.Entry(ligneFacture).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LigneFactureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LigneFactures
        [ResponseType(typeof(LigneFacture))]
        public IHttpActionResult PostLigneFacture(LigneFacture ligneFacture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LigneFactures.Add(ligneFacture);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = ligneFacture.Id }, ligneFacture);
        }

        // DELETE: api/LigneFactures/5
        [ResponseType(typeof(LigneFacture))]
        public IHttpActionResult DeleteLigneFacture(int id)
        {
            LigneFacture ligneFacture = db.LigneFactures.Find(id);
            if (ligneFacture == null)
            {
                return NotFound();
            }

            db.LigneFactures.Remove(ligneFacture);
            db.SaveChanges();

            return Ok(ligneFacture);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LigneFactureExists(int id)
        {
            return db.LigneFactures.Count(e => e.Id == id) > 0;
        }
    }
}