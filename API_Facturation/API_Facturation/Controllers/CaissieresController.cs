﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Facturation.Models;

namespace API_Facturation.Controllers
{
    public class CaissieresController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Caissieres
        public IQueryable<Caissiere> GetCaissieres()
        {
            return db.Caissieres;
        }

        // GET: api/Caissieres/5
        [ResponseType(typeof(Caissiere))]
        public IHttpActionResult GetCaissiere(int id)
        {
            Caissiere caissiere = db.Caissieres.Find(id);
            if (caissiere == null)
            {
                return NotFound();
            }

            return Ok(caissiere);
        }

        // PUT: api/Caissieres/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCaissiere(int id, Caissiere caissiere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != caissiere.Id)
            {
                return BadRequest();
            }

            db.Entry(caissiere).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CaissiereExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Caissieres
        [ResponseType(typeof(Caissiere))]
        public IHttpActionResult PostCaissiere(Caissiere caissiere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Caissieres.Add(caissiere);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = caissiere.Id }, caissiere);
        }

        // DELETE: api/Caissieres/5
        [ResponseType(typeof(Caissiere))]
        public IHttpActionResult DeleteCaissiere(int id)
        {
            Caissiere caissiere = db.Caissieres.Find(id);
            if (caissiere == null)
            {
                return NotFound();
            }

            db.Caissieres.Remove(caissiere);
            db.SaveChanges();

            return Ok(caissiere);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CaissiereExists(int id)
        {
            return db.Caissieres.Count(e => e.Id == id) > 0;
        }
    }
}